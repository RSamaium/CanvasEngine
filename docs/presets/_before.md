## Install

Please install the `@canvasengine/presets` package first.

```bash
npm install @canvasengine/presets
```

Then, you can use the presets in your project.