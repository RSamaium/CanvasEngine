# Use Canvas component

It's the starting point for all the other components.

Common example:

```html
<Canvas width="100%" height="100%" antialias="true">
    
</Canvas>
```

### options

You can use all properties from [PixiJS Canvas Renderer](https://pixijs.download/release/docs/rendering.html#autoDetectRenderer)