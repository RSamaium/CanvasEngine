# Use Sprite component

## Simple Image:

```html
<Sprite 
    image="path/to/image.png"
/>
```

## Part of an image:

```html
<Sprite 
    image="path/to/image.png"
    rectangle={{ x: 0, y: 0, width: 100, height: 100 }}
/>
```

## Sprite Sheet:

```html
<script>
const definition = {
    id: "hero",
    image: "./hero_2.png",
    width: 1248,
    height: 2016,
    framesWidth: 6,
    framesHeight: 4,
    textures: {
        stand: {
             animations: ({ direction }) => [
                [ { time: 0, 0, frameY: 0 } ]
             ]
        }
    }
}

</script>
<Sprite 
    image="path/to/image.png" 
    sheet = {
        {
            definition,
            playing: "stand",
            params: {
                direction: "right"
            }
        }
    }
/>
```

<!-- @include: ./_display-object.md -->
